import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  private _places: Place[] = [
    new Place(
      "p1", 
      "Manhattan Mansion", 
      "En el corazon de New York.",         
      "https://images.unsplash.com/photo-1416331108676-a22ccb276e35?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1347&q=80"
      , 500,
      new Date('2020-01-01'),
      new Date('2020-12-31')
    ),
    new Place(
      "p2", 
      "L'Amour Toujours", 
      "Lugar romantico en Paris!", 
      "https://images.unsplash.com/photo-1471623600634-4d04cfc56a27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80"
      , 198.99,
      new Date('2020-01-01'),
      new Date('2020-12-31')
    ),
    new Place(
      "p3", 
      "The Foggy Place", 
      "No es la tipica aventura urbana!", 
      "https://images.unsplash.com/photo-1532081000137-2bd921a97119?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1534&q=80"
      , 99,
      new Date('2020-01-01'),
      new Date('2020-12-31')
    )
  ];

  getPlaces() {
    return [... this._places];
  }

  constructor() { }

  getPlace(id: string): Place{
    let place = this._places.find( (place) => {
      if( place.id === id){
        return true;
      }
    });

    return { ... place };
  }
}
